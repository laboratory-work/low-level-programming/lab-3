#include <stdio.h>

int vectorA[] = {1, 2, 3 };
int vectorB[] = {4, 5, 6 };

int dot_product(const int *a, const int *b, size_t length) {
    int dot = 0;
    for(size_t i = 0; i < length; ++i) {
        dot += a[i] * b[i];
    }
    return dot;
}

void printArrays(const int *a, size_t size){
    for (size_t i = 0; i < size; ++i) {
        printf("%d ", a[i]);
    }
    printf("\n");
}

int main() {
    size_t aLength = sizeof(vectorA) / sizeof(int);
    size_t bLength = sizeof(vectorB) / sizeof(int);

    printf("Input arrays\n");
    printf("Vector A: ");
    printArrays(vectorA, aLength);
    printf("Vector B: ");
    printArrays(vectorB, bLength);

    int dot = dot_product(vectorA, vectorB, aLength);
    printf("Dot product: %d", dot);

    return 0;
}
