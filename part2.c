#include <stdio.h>

#define ul unsigned long

int is_prime(ul n){
    if(n < 2ul) return 0;
    for(ul i = 2ul; i < n/2ul + 1ul; ++i)
        if(!(n%i)) return 0;
    return 1;
}

int main() {
    ul num;
    printf("Please, enter as number: ");
    scanf("%lu", &num);

    if(is_prime(num) == 1)
        printf("The number %lu is prime", num);
    else
        printf("The number %lu is not prime", num);

    printf("\n");

    return 0;
}
